const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const photosRouter = require('./routes/photos');
const notFoundRouter = require('./routes/404');
const port = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());
app.use('/api/photos', photosRouter);
app.use('/', notFoundRouter);
app.listen(port, () => {
    console.warn(`app is running on port ${port}`);
});
