const text2png = require('text2png');

exports.transformPhoto = (req, res, next) => {
    const { text, options } = req.body;
    if (!text) {
        return res.status(400).json({
            success: false,
            message: 'Provide a text to parse!',
        });
    }
    const allowedOptions = [
        'font',
        'textAlign',
        'color',
        'backgroundColor',
        'lineSpacing',
        'strokeWidth',
        'strokeColor',
        'padding',
        'borderWidth',
        'border',
        'borderColor',
        'localFontPath',
        'localFontName',
        'output',
    ];
    if (!options) {
        const photo = text2png(text).toString('base64');
        const responseData = {
            success: true,
            photo,
        };
        return res.json(responseData);
    }
    const receivedOptionsEntries = Object.entries(options);
    const filteredOptions = receivedOptionsEntries.filter((option) => allowedOptions.includes(option));
    const validatedOptions = Object.fromEntries(filteredOptions);
    const encodedPhoto = text2png(text, validatedOptions).toString('base64');

    const responseData = {
        photo: encodedPhoto,
        success: true,
    };

    return res.json(responseData);
};

exports.photosHelp = (req, res, next) => {
    res.json({
        success: true,
        availableOptions: [
            'font',
            'textAlign',
            'color',
            'backgroundColor',
            'lineSpacing',
            'strokeWidth',
            'strokeColor',
            'padding',
            'borderWidth',
            'border',
            'borderColor',
            'localFontPath',
            'localFontName',
            'output',
        ],
    });
};
