const express = require('express');
const notFoundController = require('../controllers/404');

const router = express.Router();
router.use('/', notFoundController.notFound);

module.exports = router;
