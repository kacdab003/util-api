const express = require('express');

const photosController = require('../controllers/photosController');
const router = express.Router();

router.post('/transform', photosController.transformPhoto);
router.get('/transform/help', photosController.photosHelp);

module.exports = router;
